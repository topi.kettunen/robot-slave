#!/bin/sh

while true; do
        TIMESTAMP=$(date +%s%N)
        CPU_AMOUNT=$(nproc --all)

        curl -i -XPOST "http://influx:8086/write?db=robotframework" --data-binary "cpu_core_amount,host=localhost value=$CPU_AMOUNT $TIMESTAMP"

        sleep 15
done