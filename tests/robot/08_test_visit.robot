*** Settings ***
Suite Setup       Connect To Database    mysql.connector
...               ${DBName}    ${DBUser}    ${DBPass}
...               ${DBHost}    ${DBPort}
Suite Teardown    Disconnect From Database
Library           DatabaseLibrary
Metadata          SUITE_UUID  c7d6e972-af43-11e8-98d0-529269fb1459
Test Template     Check Newly Added Pet's Visits
*** Variables ***
${DBHost}         db
${DBName}         petclinic
${DBPass}         petclinic
${DBPort}         3306
${DBUser}         root

*** Test Cases ***
Test for first visit   14
  [Tags]  TEST_UUID_c27e1fb6-9bcc-11e8-98d0-529269fb1459
Test for second visit  15
  [Tags]  TEST_UUID_c27e2236-9bcc-11e8-98d0-529269fb1459
Test for third visit   16
  [Tags]  TEST_UUID_c27e2498-9bcc-11e8-98d0-529269fb1459

*** Keywords ***
Check Newly Added Pet's Visits
    [Arguments]  ${PET_ID}
    Check If Exists In Database  SELECT * FROM visits WHERE pet_id = ${PET_ID};
