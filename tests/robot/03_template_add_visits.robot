*** Settings ***
Suite Setup       Suite Setup
Suite Teardown    Suite Teardown
Resource          ../resources/navigation.robot
Resource          ../resources/resource.robot
Metadata          SUITE_UUID  c8db8ed8-af41-11e8-98d0-529269fb1459
Test Template     Save three new visits

*** Test Cases ***
Add visit for Matti  ${SERVER}/owners/11/pets/14/visits/new  2015-03-21  Vaccine
  [Tags]  TEST_UUID_7c94b696-9bb7-11e8-98d0-529269fb1459
Add visit for Teppo  ${SERVER}/owners/11/pets/15/visits/new  2015-05-22  Is earth flat or round
  [Tags]  TEST_UUID_7c94b998-9bb7-11e8-98d0-529269fb1459
Add visit for Seppo  ${SERVER}/owners/11/pets/16/visits/new  2019-07-26  Meat is murder
  [Tags]  TEST_UUID_7c94baf6-9bb7-11e8-98d0-529269fb1459

*** Keywords ***
Save three new visits
    [Arguments]  ${pet_path}    ${date}   ${description}
    Go To        ${pet_path}
    Input text    date          ${date}
    Input text    description   ${description}
    Click Button  Add Visit

