*** Settings ***
Suite Setup       Connect To Database    mysql.connector
...               ${DBName}    ${DBUser}    ${DBPass}
...               ${DBHost}    ${DBPort}
Suite Teardown    Disconnect From Database
Library           DatabaseLibrary
# Library           OperatingSystem
Metadata          SUITE_UUID  c7d6dd10-af43-11e8-98d0-529269fb1459

*** Variables ***
${DBHost}         db
${DBName}         petclinic
${DBPass}         petclinic
${DBPort}         3306
${DBUser}         root
${UUID1}  
${UUID2}  
${QUERY_FOR_JUHA_BY_FIRSTNAME}  SELECT * FROM owners
...                             WHERE first_name = 'Juha';
${QUERY_FOR_ALL_PETS}           SELECT * FROM pets;

*** Test Cases ***

Check If Exists Juha
    Check If Exists In Database  ${QUERY_FOR_JUHA_BY_FIRSTNAME}
    [Tags]  TEST_UUID_c27e1750-9bcc-11e8-98d0-529269fb1459

Check All Pets
    Check If Exists In Database  ${QUERY_FOR_ALL_PETS}
    [Tags]  TEST_UUID_c27e1a20-9bcc-11e8-98d0-529269fb1459

