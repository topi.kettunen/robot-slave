*** Settings ***
Suite Setup       Suite Setup
Suite Teardown    Suite Teardown
Resource          ../resources/navigation.robot
Resource          ../resources/resource.robot
Test Template     Save three new owners
Metadata          SUITE_UUID  c8db8ab4-af41-11e8-98d0-529269fb1459

*** Test Cases ***
Add owner Paavo   Paavo  Rohamo    Paavontie 1  Helsinki  0400123456
  [Tags]  TEST_UUID_7c94c58c-9bb7-11e8-98d0-529269fb1459
Add owner Juha    Juha   Lääperi   Juhantie 2   Helsinki  0400987654
  [Tags]  TEST_UUID_7c94c44c-9bb7-11e8-98d0-529269fb1459
Add owner Emma    Emma   Lepistö   Emmantie 3   Helsinki  0400546938
  [Tags]  TEST_UUID_7c94c2d0-9bb7-11e8-98d0-529269fb1459

*** Keywords ***
User navigates to add new owner
    navigation.User navigates to find owners page
    Click Link  Add Owner

User saves new owner's info
    Click Button  Add Owner

Save three new owners
    User navigates to add new owner
    [Arguments]   ${FIRSTNAME}   ${LASTNAME}   ${ADDRESS}   ${CITY}   ${TELEPHONE}
    Input text    firstName  ${FIRSTNAME}
    Input text    lastName   ${LASTNAME}
    Input text    address    ${ADDRESS}
    Input text    city       ${CITY}
    Input text    telephone  ${TELEPHONE}
    User saves new owner's info