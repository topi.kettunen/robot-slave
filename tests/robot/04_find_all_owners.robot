*** Settings ***
Suite Setup       Suite Setup
Suite Teardown    Suite Teardown
Resource          ../resources/navigation.robot
Resource          ../resources/resource.robot
Metadata          SUITE_UUID  c8db914e-af41-11e8-98d0-529269fb1459

*** Variables ***
${PAGE_TITLE}    Owners
${ALL_OWNERS_PAGE_LINK}       xpath=//html/body/div/div/form/div[2]/div/button
${UUID1}  

*** Test Cases ***
User navigates to all owners page
    When User navigates to find owners page
    and User navigates to all owners page
    Then User should see appropriate title
    [Tags]  TEST_UUID_c27e0f6c-9bcc-11e8-98d0-529269fb1459