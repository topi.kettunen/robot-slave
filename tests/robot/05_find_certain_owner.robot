*** Settings ***
Suite Setup       Suite Setup
Suite Teardown    Suite Teardown
Resource          ../resources/navigation.robot
Resource          ../resources/resource.robot
Metadata          SUITE_UUID  c8db927a-af41-11e8-98d0-529269fb1459

*** Variables ***
${TITLE}        xpath=//h2

*** Test Cases ***
User navigates to find certain owner's page
    When User navigates to find owners page
    Then User searches for certain owner
    [Tags]  TEST_UUID_c27e12b4-9bcc-11e8-98d0-529269fb1459
