import requests
from robot.api import logger

class TestListener:
    ROBOT_LISTENER_API_VERSION = 3

    def end_test(self, data, test):
        if test.status == 'FAIL':
            uuid = ''.join(test.tags)
            requests.post('http://rest:5000/quarantine/tests', data={'uuid': uuid})
        elif test.status == 'PASS':
            uuid = ''.join(test.tags)
            requests.delete('http://rest:5000/quarantine/tests', data={'uuid': uuid})
