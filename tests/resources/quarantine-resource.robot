*** Settings ***
Library      SeleniumLibrary
Library      Remote  http://xvfb:8270  WITH NAME  xvfb
Library      Remote  http://i3wm:8270  WITH NAME  i3wm
Library      Remote  http://ffmpeg:8270  WITH NAME  ffmpeg

*** Variables ***
${SERVER}      http://petclinic:8080
${BROWSER}    Firefox
${DELAY}      0   

*** Keywords ***
Suite Setup
    Open Display
    Sleep    3
    Start Window Manager
    Start Capture
    Open Browser  ${SERVER}  ${BROWSER}
    Set Selenium Speed  ${DELAY}

Suite Teardown
    Close Browser
    Stop Capture
    Stop Window Manager
    Close Display
